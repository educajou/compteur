# compteur

## Description

Cette application affiche un compteur et permet d'ajouter/soustraire une ou plusieurs unitées, dizaines, centaines.

## Utilisation

Téléchargez le projet et lancez index.html

Ou bien utilisez la version en ligne: https://educajou.forge.apps.education.fr/compteur/

## Fonctionnement

Pour faire fonctionner le compteur, utilisez les boutons au-dessus et en-dessous des chiffres. Ce compteur fonctionne uniquement avec des nombres positifs, donc vous ne pouvez pas, par exemple, enlever une dizaine si le nombre affiché est inférieur à 10. Mais si par exemple le nombre est 100, il est possible d'enlever une dizaine, ce qui cassera la centaine.

Pour ajouter une colonne d'entiers, utilisez le bouton dédié vert à gauche.

Pour ajouter une colonne de décimales, utilisez le bouton dédié vert à droite.

Pour ajouter / soustraire plusieurs unités, plusieurs dizaines ... utilisez le panneau du haut : choisissez + ou -, ainsi que le nombre choisi, et cliquez sur OK. (Vous pouvez régler les options de la liste déroulante afin de ne faire apparaître que celles qui sont pertinentes pour vos élèves.)

Vous pouvez lancer l'application Compteur en ajoutant le nombre à la fin de l'URL, par exemple :

https://educajou.forge.apps.education.fr/compteur/?nombre=10,25

ou encore https://educajou.forge.apps.education.fr/compteur/?nombre=35,00, ce qui forcera deux décimales nulles.

Autre exemple : https://educajou.forge.apps.education.fr/compteur/?nombre=000265 pour afficher le nombre 265 ainsi que les colonnes des milliers quoique nulles.

## Licence

Compteur est développée par Arnaud Champollion. Elle est sous licence libre GNU/GPL.

Cette application fait partie de la suite Éducajou et elle est partagée sur la Forge des Communs Numériques de Apps Éducation.

Voir les sources sur la page Compteur de la Forge.

## Crédits

Remerciements à Olivier Jeulin pour la création des icônes "ajouter/supprimer une colonne", sous licence Crative Commons BY SA 4.0.
